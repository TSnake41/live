-- Specialized physics menues.
local format = string.format

return function (lynx, make_tree)
  local component_color = { .4, .4, .4, 1 }
  local green_color = { 0, 1, 0, 1 }
  local red_color = { 1, 0, 0, 1 }

  local function alignl(list)
    for i,v in ipairs(list) do
      v.align = "left"
    end

    return list
  end

  local function make_multi_getter(f, arg)
    return function (self)
      return select(arg, f(self))
    end
  end

  -- Getter/Setter update function (similar to observer.update)
  local gs_observer_update = function (self, menu, dt)
    lynx.text.update(self, menu, dt)

    if not self.selected or self.continous then -- allow merging with editors
      local value = self.get(self.variable)

      if self.value ~= value then
        self.value = value
        self.text = self:format_fn(self.selected)
      end
    end
  end

  local gs_observer_event = function (self, menu, value)
    if self.set then
      self.set(self.variable, value)
    end
  end

  local function gs_slider(variable, name, get, set)
    local event = set and gs_observer_event

    return lynx.slider(event, {
      variable = variable, allow_repeat = true, continous = true,
      min_value = -math.huge, max_value = math.huge, step = .1,
      format = name .. ": %g", update = gs_observer_update,
      get = get, set = set
    })
  end

  local function gs_observer(variable, name, get, format_fn)
    local default_format = function (self)
      return format(name .. ": %g", self.value)
    end

    return lynx.text(name, {
      variable = variable, update = gs_observer_update, get = get,
      format_fn = format_fn or default_format
    })
  end

  local function gs_observer_bool(variable, name, get)
    return gs_observer(variable, name, get, function (self)
      self.text_color = self.value and green_color or red_color
      return name
    end)
  end


  local back_button = lynx.button("<<<", function (self, menu)
    menu:pop()
  end, {
    align = "left"
  })

  local World = function (world, name)
    return alignl {
      back_button,
      lynx.text(name .. " [Box2D World]", {
        selectable = false,
        text_color = { .5, .7, .7, 1 }
      }),

      lynx.text("", { selectable = false }),

      lynx.text("[Gravity]", { selectable = false, text_color = component_color }),
      gs_observer(world, "x", make_multi_getter(world.getGravity, 1)),
      gs_observer(world, "y", make_multi_getter(world.getGravity, 2)),

      lynx.text("", { selectable = false }),

      lynx.text("[Bodies]", { selectable = false, text_color = component_color }),
      gs_observer(world, "count", world.getBodyCount),
      lynx.button("list", function (self, menu)
        menu:push { items = make_tree(world:getBodies(), name .. "'s bodies") }
      end),

      lynx.text("", { selectable = false }),

      lynx.text("[Contacts]", { selectable = false, text_color = component_color }),
      gs_observer(world, "count", world.getContactCount),
      lynx.button("list", function (self, menu)
        menu:push { items = make_tree(world:getContacts(), name .. "'s contacts") }
      end),

      lynx.text("", { selectable = false }),

      lynx.text("[Joints]", { selectable = false, text_color = component_color }),
      gs_observer(world, "count", world.getJointCount),
      lynx.button("list", function (self, menu)
        menu:push { items = make_tree(world:getJoints(), name .. "'s joints") }
      end),

    }
  end

  local Body = function (body, name)
    return alignl {
      back_button,
      lynx.text(name .. " [Box2D Body]", {
        selectable = false,
        text_color = { .5, .7, .7, 1 }
      }),

      lynx.button("world", function (self, menu)
        menu:push { items = World(body:getWorld(), name .. "'s world") }
      end, { text_color = { .3, .5, .5, 1 } }),

      lynx.text("", { selectable = false }),

      gs_observer(body, "type", body.getType, function (self)
        return self.value
      end),
      gs_observer_bool(body, "active", body.isActive),
      gs_observer_bool(body, "awake", body.isAwake),
      gs_observer_bool(body, "fixed rotation", body.isFixedRotation),

      lynx.text("", { selectable = false }),

      lynx.text("[Position]", { selectable = false, text_color = component_color }),
      gs_slider(body, "x", body.getX, body.setX),
      gs_slider(body, "y", body.getY, body.setY),
      gs_slider(body, "angle", body.getAngle, body.setAngle),

      lynx.text("", { selectable = false }),

      lynx.text("[Velocity]", { selectable = false, text_color = component_color }),
      gs_observer(body, "x", make_multi_getter(body.getLinearVelocity, 1)),
      gs_observer(body, "y", make_multi_getter(body.getLinearVelocity, 2)),
      gs_observer(body, "angle", body.getAngularVelocity),

      lynx.text("", { selectable = false }),

      lynx.text("[Properties]", { selectable = false, text_color = component_color }),
      gs_observer(body, "mass", body.getMass),
      gs_observer(body, "damping", body.getLinearDamping),
      gs_observer(body, "inertia", body.getInertia),
    }
  end

  local Shape = function (shape, name)
    return alignl {
      back_button,
      lynx.text(name .. " [Box2D Shape]", {
        selectable = false,
        text_color = { .5, .7, .7, 1 }
      }),
    }
  end

  local Fixture = function (fixture, name)
    return alignl {
      back_button,
      lynx.text(name .. " [Box2D Fixture]", {
        selectable = false,
        text_color = { .5, .7, .7, 1 }
      }),

      lynx.button("body", function (self, menu)
        menu:push { items = Body(fixture:getBody(), name .. "'s body") }
      end, { text_color = { .3, .5, .5, 1 } }),

      lynx.button("shape", function (self, menu)
        menu:push { items = Shape(fixture:getShape(), name .. "'s shape") }
      end, { text_color = { .3, .5, .5, 1 } }),

      lynx.text("", { selectable = false }),

      gs_observer(fixture, "category", fixture.getCategory),
      gs_observer_bool(fixture, "sensor", fixture.isSensor),

      lynx.text("", { selectable = false }),

      lynx.text("[Properties]", { selectable = false, text_color = component_color }),
      gs_slider(fixture, "friction", fixture.getFriction, fixture.setFriction),
      gs_slider(fixture, "density", fixture.getDensity, fixture.setDensity),
      gs_slider(fixture, "restitution", fixture.getRestitution, fixture.setRestitution),
    }
  end

  local Joint = function (body, name)
    return alignl {
      back_button,
      lynx.text(name .. " [Box2D Body]", {
        selectable = false,
        text_color = { .5, .7, .7, 1 }
      }),

      lynx.button("world", function (self, menu)
        menu:push { items = World(body:getWorld(), name .. "'s world") }
      end, { text_color = { .3, .5, .5, 1 } }),

      lynx.text("", { selectable = false }),

      gs_observer(body, "type", body.getType, function (self)
        return self.value
      end),

      gs_observer_bool(body, "active", body.isActive),
      gs_observer_bool(body, "awake", body.isAwake),
      gs_observer_bool(body, "fixed rotation", body.isFixedRotation),

      lynx.text("", { selectable = false }),

      lynx.text("[Position]", { selectable = false, text_color = component_color }),
      gs_slider(body, "x", body.getX, body.setX),
      gs_slider(body, "y", body.getY, body.setY),
      gs_slider(body, "angle", body.getAngle, body.setAngle),

      lynx.text("", { selectable = false }),

      lynx.text("[Velocity]", { selectable = false, text_color = component_color }),
      gs_observer(body, "x", make_multi_getter(body.getLinearVelocity, 1)),
      gs_observer(body, "y", make_multi_getter(body.getLinearVelocity, 2)),
      gs_observer(body, "angle", body.getAngularVelocity),

      lynx.text("", { selectable = false }),

      lynx.text("[Properties]", { selectable = false, text_color = component_color }),
      gs_observer(body, "mass", body.getMass),
      gs_observer(body, "damping", body.getLinearDamping),
      gs_observer(body, "inertia", body.getInertia),
    }
  end

  return { {
      filter = function (obj)
        return type(obj) == "userdata" and obj.type and obj:typeOf "Body"
      end,
      handler = function (var, k)
        return lynx.button(format("%s: %s", k, tostring(var[k])),
          function (self, menu)
            menu:push { items = Body(var[k], tostring(k)) }
          end)
      end
    }, {
      filter = function (obj)
        return type(obj) == "userdata" and obj.type and obj:typeOf "World"
      end,
      handler = function (var, k)
        return lynx.button(format("%s: %s", k, tostring(var[k])),
          function (self, menu)
            menu:push { items = World(var[k], tostring(k)) }
          end)
      end
    }, {
      filter = function (obj)
        return type(obj) == "userdata" and obj.type and obj:typeOf "Fixture"
      end,
      handler = function (var, k)
        return lynx.button(format("%s: %s", k, tostring(var[k])),
          function (self, menu)
            menu:push { items = Fixture(var[k], tostring(k)) }
          end)
      end
    } }
end
