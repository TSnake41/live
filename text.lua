local text = {}

text.background = { .8, .8, .8, 0.5 }
text.box_background = { .25, .25, .25, 0.75 }
text.foreground = { 1, 1, 1, 1 }
text.enabled = false
text.hint = ""
text.text = "hello world !"

function text:get(hint, event, input_handler)
  love.keyboard.setTextInput(true)

  if not self.font then
    self.font = love.graphics.newFont(14)
  end

  self.enabled = true

  if hint then
    self.hint = hint
  end

  self.event = event
  self.input_handler = input_handler
  self.text = ""
end

function text:textinput(text)
  self.text = self.text .. text
end

function text:update()
  if self.enabled then
    if self.input_handler:pressed "back" then
      self.enabled = false
    end

    if self.input_handler:pressed "enter" then
      self.event(self.text)
      self.enabled = false
    end
  end
end

function text:draw()
  if self.enabled then
    local w, h = love.graphics.getDimensions()

    love.graphics.setColor(self.background)
    love.graphics.rectangle("fill", 0, 0, w, h)

    love.graphics.setColor(self.box_background)
    love.graphics.rectangle("fill", w / 2 - 100, h / 2 - 20, 200, 20)

    love.graphics.setColor(self.foreground)
    love.graphics.setFont(self.font)
    love.graphics.print(self.text, w / 2 - 100 + 2, h / 2 - 20 + 2)
  end
end

return text
