local base = (...)
local lynx = require(base .. ".lynx")
local handlers

local live = {}

local input = require(base .. ".baton").new {
  controls = {
    left = {'key:left', 'key:a', 'axis:leftx-', 'button:dpleft'},
    right = {'key:right', 'key:d', 'axis:leftx+', 'button:dpright'},
    up = {'key:up', 'key:w', 'axis:lefty-', 'button:dpup'},
    down = {'key:down', 'key:s', 'axis:lefty+', 'button:dpdown'},
    enter = {'key:return', 'button:a'},
    back = { 'key:backspace', 'button:b' },
    scrollkey = { 'key:lctrl', 'button:y' },
    reset = { 'key:r', 'button:back' },
    boost = { 'key:lshift' }
  },
  pairs = {
    move = {'left', 'right', 'up', 'down'}
  },
  joystick = love.joystick.getJoysticks()[1]
}

local function make_tree(var, name)
  if type(var) ~= "table" then
    var = { value = var }
  end

  local name = string.format("%s [%s]", name, type(var))

  local list = {
    lynx.button("(hide)", function (self, menu)
      menu:push {
        items = {
          lynx.button("(show)", function (self, menu)
            menu:pop()
          end, { align = "left" })
        }
      }
    end),
    lynx.text(name, {
      selectable = false,
      text_color = { .5, .7, .7, 1 }
    }),
    lynx.button("<<<", function (self, menu) menu:pop() end),
    { selectable = false }
  }

  for k,v in pairs(var) do
    for i,h in ipairs(handlers) do
      if h.filter(v) then
        list[#list + 1] = h.handler(var, k)
        break
      end
    end
  end

  for i,v in ipairs(list) do
    v.align = "left"
  end

  return list
end

function live:new(width, height, vars, font)
  local self = setmetatable({}, live.mt)

  self.w = width
  self.h = height

  self.font = font or love.graphics.newFont(15)
  self.enabled = false

  self.canvas = love.graphics.newCanvas(width, height)
  self.font_height = font and font:getHeight() or 20
  self.speed = 1

  vars.live = self

  self.menu = lynx.menu(make_tree(vars, "root"), {
    x = 0, y = 0,
    ox = 5, oy = self.font_height * 2 + 10,
    w = width, h = height - 30,
    default_height = self.font_height,
    funcs = lynx.love_lynx
  })

  self.show_live = true
  self.show_stats = true
  self.use_canvas = false

  return self
end

function live:update(dt)
  if self.enabled then
    input:update()

    if input:down "scrollkey" then
      self.moving = true
      if input:pressed "reset" then
        self.menu.ox = 5
        self.menu.oy = self.font_height * 2 + 10
      end

      self.speed = input:down "boost" and 5 or 1

      local x, y = input:get "move"

      self.menu.ox = self.menu.ox + x * dt * 200 * self.speed
      self.menu.oy = self.menu.oy + y * dt * 200 * self.speed
    else
      self.moving = false

      if input:pressed "back" then
        self.menu:pop()
      end

      for k,v in pairs(input.config.controls) do
        if input:pressed(k) then
          self.menu:input_key(k, "pressed")
        end

        if input:down(k) then
          self.menu:input_key(k, "down")
        end
      end
    end
  end

  --text:update()
  self.menu:update(dt)
end

function live:mousemoved(x, y)
  if self.enabled and not self.moving then
    self.menu:input_mouse(x, y, 0)
  end
end

function live:mousepressed(x, y, btn)
  if self.enabled and not self.moving then
    if btn == 2 then
      self.menu:pop()
    else
      self.menu:input_mouse(x, y, btn)
      self.menu:input_mouse(x, y, 0) -- can fix positioning issues
    end
  end
end

function live:draw(x, y)
  if self.use_canvas or self.enabled then

  if self.use_canvas then
    local old_canvas = love.graphics.getCanvas()
  end

  local old_r, old_g, old_b, old_a = love.graphics.getColor()
  local old_font = love.graphics.getFont()

  if self.use_canvas then
    love.graphics.setCanvas(self.canvas)
    love.graphics.clear(0, 0, 0, 0)
  end

  love.graphics.setColor(0, 0, 0, 0.5)
  if self.enabled then
    love.graphics.rectangle("fill", 0, 0, self.w, self.h)
  end

  love.graphics.setColor(.1, .1, .1, .25)

  if self.moving then
    love.graphics.setColor(.75, .75, .75, .5)
    love.graphics.rectangle("fill", 0, 0, self.w, self.h)

    love.graphics.setColor(1, 1, 1, 1)
    love.graphics.print(string.format("X:%.1f Y:%.1f", self.menu.ox, self.menu.oy), 5, 45)
  end

  love.graphics.setFont(self.font)
  self.menu:draw()

  if self.show_live then
    love.graphics.setColor(.25, .5, 1, 1)
    love.graphics.print("LIVE Integrated Variable Explorer", 5, 5)
  end

  if self.show_stats then
    love.graphics.setColor(.75, .5, 1, 1)
    love.graphics.print(
      string.format("FPS: %g | Memory: %g kb", love.timer.getFPS(),
      math.ceil(collectgarbage 'count')), 5, self.font_height + 5
    )
  end

  if self.use_canvas then
    love.graphics.setCanvas(old_canvas)

    love.graphics.setColor(1, 1, 1, self.enabled and 1 or 0.5)
    love.graphics.draw(self.canvas, x, y)
  end

  love.graphics.setFont(old_font)
  love.graphics.setColor(old_r, old_g, old_b, old_a)
  end
end

handlers = require(base .. ".handlers")(lynx, make_tree)
live.make_tree = make_tree
live.handlers = handlers

live.mt = {
  __index = live,
}

live.__call = live.new
return setmetatable(live, live)
