local format = string.format

return function (lynx, make_tree)
  local back_button = lynx.button("<<<", function (self, menu)
    menu:pop()
  end, {
    align = "left"
  })

  return { {
    -- Drawables (most)
    filter = function (obj)
      return type(obj) == "userdata" and obj.type and obj:typeOf "Drawable"
    end,
    handler = function (var, k)
      return lynx.button(format("%s: %s", k, tostring(var[k])),
        function (self, menu)
          menu:push {
            items = {
              back_button,
              {
                draw = function (self, menu, x, y)
                  love.graphics.setColor(1, 1, 1, 1)
                  pcall(love.graphics.draw, var[k], x, y)
                end,
                height = var[k].getHeight and var[k]:getHeight()
              }
            }, current = 1
          }
        end)
    end
  }, {
    -- Fonts
    filter = function (obj)
      return type(obj) == "userdata" and obj.type and obj:typeOf "Font"
    end,
    handler = function (var, k)
      return lynx.button(format("%s: %s", k, tostring(var[k])),
        function (self, menu)
          menu:push {
            items = {
              lynx.button("<<<", function (self, menu) menu:pop() end, { align = "left" }),
              {
                draw = function (self, menu, x, y, w, h)
                  love.graphics.setColor(1, 1, 1, 1)
                  pcall(function (f, x, y)
                    local font = love.graphics.getFont()
                    love.graphics.setFont(f)
                    love.graphics.print("The quick brown fox jumps over the lazy dog", x, y)
                    love.graphics.setFont(font)
                  end, var[k], x, y)
                end,
                height = var[k]:getHeight() * 2
              }
            }, current = 1
          }
        end)
    end
  } }
end
