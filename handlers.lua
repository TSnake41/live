local base = (...)
local format = string.format

local function type_filter(t)
  return function (obj)
    return type(obj) == t
  end
end

local function get_name(obj)
  -- Check if it has a name
  local status, name = pcall(function () return var[k].name end)
  if not status then
    name = nil -- Don't use error as name
  end

  return name or ""
end

--[[
  Handlers: {
    filter = function (obj) -> boolean,
    handler = function (var, key) -> lynx item
  }
]]
return function (lynx, make_tree)
  local back_button = lynx.button("<<<", function (self, menu)
    menu:pop()
  end, {
    align = "left"
  })

  local handlers = {
    {
      -- __live metamethod
      filter = function (obj)
        local mt = getmetatable(obj)
        return mt and mt.__live
      end,
      handler = function (var, k)
        return lynx.button(format("%s: %s>", tostring(k), get_name(var[k])),
          function (self, menu)
            local __live = getmetatable(var[k]).__live
            menu:push(type(__live) == "function" and __live(var[k]) or __live)
          end)
      end
    }, {
      -- Tables
      filter = type_filter "table",
      handler = function (var, k)
        return lynx.button(format("%s: %s >>>", tostring(k), get_name(var[k])),
          function (self, menu)
            menu:push { items = make_tree(var[k], tostring(k)), current = 3 }
          end)
      end
    }, {
      -- Numbers
      filter = type_filter "number",
      handler = function (var, k)
        return lynx.slider(function (self, menu, value)
          self.variable[self.key] = value
        end, {
          min_value = -math.huge, max_value = math.huge, step = .25, value = var[k],
          update = lynx.observer.update, variable = var, key = k,
          allow_repeat = true, format = tostring(k) ..": %g", continous = true
        })
      end
    }, {
      -- Functions
      -- Calls var:[k]()
      filter = type_filter "function",
      handler = function (var, k)
        return lynx.button(tostring(k) .. ": ()", function(self, menu)
          pcall(function ()
            local ret = { var[k](var) }

            if #ret > 0 then
              menu:push { items = make_tree(ret) }
            end
          end)
        end)
      end
    }, {
      filter = type_filter "boolean",
      handler = function (var, k)
        return lynx.button("", function (self)
          self.variable[self.key] = not self.variable[self.key]
        end, {
          update = lynx.observer.update, variable = var, key = k,
          format = tostring(k) .. ": %s", format_fn = function (self)
            return format("%s: %s", k, tostring(var[k]))
          end
        })
      end
    }
  }

  -- Drawables handler
  for _,v in ipairs(require(base .. '.drawables')(lynx, make_tree)) do
    handlers[#handlers+1] = v
  end

  -- Physics handler
  for _,v in ipairs(require(base .. '.physics')(lynx, make_tree)) do
    handlers[#handlers+1] = v
  end

  handlers[#handlers+1] = {
    filter = function () return true end,
    handler = lynx.observer
  }

  return handlers
end
